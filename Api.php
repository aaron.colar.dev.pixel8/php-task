<?php

header('Content-type: text/plain');

class Api {

    function printFullname($name) {
        echo "Fullname: $name \n\n";
    }

    function printHobbies($hobbies) {
        echo "Your Hobbies are: \n";
        foreach($hobbies as $hobby) {
            echo "$hobby \n";
        }
        echo "\n\n";
    }

    function printOtherData($age, $email, $birthday) {
        echo "Age: $age \n\n";
        echo "Email: $email \n\n";
        echo "Birthday: $birthday \n\n";
    }

}

$api = new Api();
$api->printFullname("aaron colar");
$api->printHobbies(["reading", "writing", "singing"]);
$api->printOtherData(1, "colar.aaron@gmail.com", "03-10-1991");

?>